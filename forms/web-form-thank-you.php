<?php include "../includes/header.php" ?>

<?php
 $name = $_POST['txtName'];
 $title = $_POST['txtTitle'];
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4 col-md-3"></div>
            <div class="col-xs-8 col-md-6">
                <h2 align="center">Thank You <?php echo $name ?></h2>
                <p align="center">We will be reviewing your feedback of <?php echo $title ?></p>
                <p align="center">We will contact you if we have any further questions.</p>
            </div>
            <div class="col-xs-4 col-md-3"></div>
        </div>
    </div>

<?php include "../includes/footer.php" ?>