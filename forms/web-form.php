<?php include "../includes/header.php" ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-4 col-md-3"></div>
        <div class="col-xs-8 col-md-6"><h1 align="center">We Value Your Feedback for the Web</h1></div>
        <div class="col-xs-4 col-md-3"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-md-3"></div>
        <div class="col-xs-8 col-md-6">
            <form action="web-form-thank-you.php" method="post">
                <div class="form-group">
                    <label for="lblName">What is Your Name?</label>
                    <input type="text" class="form-control" name="txtName" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="lblDescription">Enter a Title for Your Feedback</label>
                    <input type="text" class="form-control" name="txtTitle" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="lblDetails">Enter Feedback Details</label>
                    <select class="form-control">
                        <option disabled selected value> -- select an option -- </option>
                        <option>New Idea</option>
                        <option>Web Improvement</option>
                        <option>Incorrect Data</option>
                        <option>Missing Data</option>
                        <option>Something is Broken</option>
                        <option>Other</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="lblDetails">Enter Feedback Details</label>
                    <textarea class="form-control" rows="4" id="txtDetails"></textarea>
                </div>
                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
            </form>
        </div>
        <div class="col-xs-4 col-md-3"></div>
    </div>
</div>


<?php include "../includes/footer.php" ?>
