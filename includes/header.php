<!doctype html>
<html lang="en" xmlns:style="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Central Web Ideas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/styles.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

</head>
<body>
<!--Head Container-->
<div class="container-fluid header">
    <div class="row">
        <div class="col-xs-12 col-md-8">Welcome to Central Web Testing</div>
        <div class="col-xs-6 col-md-4">
            <img src="../assets/images/support.jpg" class="img-responsive" alt="Support Logo" style="float:right; padding:2px;"/>
        </div>
    </div>
</div>

