<?php include "includes/header.php" ?>
<?php include "includes/dbFunctions.php" ?>


<div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Title</th>
          <th scope="col">Submitted By</th>
          <th scope="col">Description</th>
          <th scope="col">Deadline</th>
        </tr>
      </thead>
     <tbody>
     <?php
     readRows();
     ?>
    </tbody>
    </table>
</div>
<?php include "includes/footer.php" ?>