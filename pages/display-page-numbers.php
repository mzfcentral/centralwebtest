<?php
ini_set('display_errors', 0);
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
?>

<?php include "includes/header.php" ?>

<?php

/* connecting to database */
$connection = mysqli_connect('localhost','root','Central20', 'page_numbers');

/* displays error if connection fails */
if (!$connection) {
    die("Database connection failed");
    echo "You did not connect successfully :(";
}

?>

<!-- initial table creation -->
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Base 6</th>
            <th scope="col">Copy Block</th>
            <th scope="col">PLine</th>
            <th scope="col">CM</th>
            <th scope="col">Master Page#</th>
            <th scope="col">School Page#</th>
            <th scope="col">Free Shipping Page#</th>
            <th scope="col">Next Day Page#</th>
        </tr>
        </thead>
        <tbody>
        <?php
        readRows();
        ?>
        </tbody>
    </table>
</div>



<?php

function readRows() {

    /* accessing connection */
    global $connection;

    /* creating MySQL queries */
    $searchBy = "'";
    $searchBy .= $_POST['searchBy'];
    $searchBy .= "'";
    $b6query = "SELECT * FROM catalog_pages WHERE Base6 = $searchBy";
    $cbquery = "SELECT * FROM catalog_pages WHERE CopyBlock = $searchBy";
    global $myQuery;

    /* determining which query to use based on selection made on form and setting the query equal to $myQuery */
    if ($_POST['search-method'] == "base6") {
        $myQuery .= $b6query;
    }
    elseif ($_POST['search-method'] == "copyblock") {
        $myQuery .= $cbquery;
    }
    else {
        echo 'Your search returned no results.';
    }


    /* querying the database */
    $result = mysqli_query($connection, $myQuery);

    /* display message if not results are returned */

    if (!$result) {
        die('Your search returned no results.') . mysqli_error();
    }
?>
    <a href="https://www.centralrestaurant.com/page-numbers">New Search</a>

<?php
    /* loop through results and display each row in table */
    while($row = mysqli_fetch_assoc($result)) { ?>
        <tr>
            <td><?php echo $row['Base6']; ?></td>
            <td><?php echo $row['CopyBlock']; ?></td>
            <td><?php echo $row['Pline']; ?></td>
            <td><?php echo $row['CM']; ?></td>
            <td><?php echo $row['MasterPage']; ?></td>
            <td><?php echo $row['SchoolPage']; ?></td>
            <td><?php echo $row['FreeShippingPage']; ?></td>
            <td><?php echo $row['NextDayPage']; ?></td>
        </tr>

        <?php

    }}

?>




<?php include "includes/footer.php" ?>
