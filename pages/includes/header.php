
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>

        body {
            background-color: #f0f0f0 !important;
        }
        /* form styles */
        form {
            background-color: #fff;
            padding: 50px;
            margin-top: 100px;
        }
        label {
            margin-top: 20px !important;
            font-size: 18px !important;
        }
        span.required-label {
            margin-left: 2.5px!important;
            font-size: 19.5px;
            color: #d63246;
        }

        div#thank-you {
            text-align: center;
            margin: 200px;
        }

        input#submit {
            margin-top: 40px;
            width: 200px;
        }

        th {
            height: 100px;
            font-size: 20px;
        }

        tr {
            height: 50px;
        }

        td {
            vertical-align: middle !important;
        }

        a {
            background-color: #00a651;
            text-decoration: none !important;
            color: #fff;
            margin: 0 auto;
            text-align: center;
            display: block;
            width: 150px;
            line-height: 2;
            font-size: 18px;
            margin-top:100px;
        }

    </style>

    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>

</head>
<body id="home">
	<div class="container">
