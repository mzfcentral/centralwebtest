<?php
/**
 * Created by PhpStorm.
 * User: anw
 * Date: 2/4/19
 * Time: 12:03 PM
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

$connection = mysqli_connect('localhost','root','Central20', 'testdb');

if (!$connection) {
    die("Database connection failed");
    echo "You did not connect successfully :(";
}

function createRows() {
    if(isset($_POST['submit'])) {
        echo 'Submit button works!';
        global $connection;
        $submittedBy=$_POST['name'];
        $idea=$_POST['idea'];
        $desc=$_POST['description'];
        $deadline=$_POST['deadline'];
        $duedate = $_POST['duedate'];

        $query = "INSERT INTO web_submissions (SubmittedBy, Idea, Description, DeadlineYN, DueDate) 
                 VALUES ";
        $query .= "('$submittedBy', '$idea', '$desc', '$deadline', '$duedate')";



        $result = mysqli_query($connection, $query);
        if (!$result) {

            die('Query FAILED') . mysqli_error();
            echo "You did not connect successfully";
        } else {
            echo "Record created";
        }
    }
}


function readRows() {
    global $connection;
    $myQuery = "SELECT * FROM web_submissions";
    $result = mysqli_query($connection, $myQuery);
    if (!$result) {
        die('Query FAILED') . mysqli_error();
    }
    while($row = mysqli_fetch_assoc($result)) { ?>
        <tr>
            <th scope="row"><?php echo $row['Idea']; ?></th>
            <td><?php echo $row['SubmittedBy']; ?></td>
            <td><?php echo $row['Description']; ?></td>
            <td><?php echo $row['DueDate']; ?></td>
        </tr>
<?php

    }
}


?>