<?php include "includes/header.php" ?>


<form action="thank-you.php" method="post">
    <div class="form-goup">
        <h1 style="text-align: center">Web Submissions Form</h1>
        <label for="name">Name</label>
        <span class="required-label">*</span>
        <input name="name" type="text" id="name" class="form-control input-lg" required>
    </div>
    <div class="form-goup">
        <label for="idea">Please give a title for your idea:</label>
        <span class="required-label">*</span>
        <input name="idea" type="text" id=idea" class="form-control input-lg" required>
    </div>
    <div class="form-goup">
        <label for="desc">Please provide a description of your idea:</label>
        <span class="required-label">*</span>
        <textarea name="description" type="textarea" id="desc" rows="10" class="form-control input-lg" required></textarea>
    </div>
    <div class="form-goup">
        <label for="deadline">Is there a deadline?</label><span class="required-label">*</span><br>
        <input name="deadline" type="radio" id="yes" value="yes" required>Yes<br>
        <input name="deadline" type="radio" id="no" value="no" required>No<br>
    </div>
    <br>
    <div class="form-goup" id="conditional">
        <label for="duedate">What is the deadline?</label>
        <span class="required-label">*</span>
        <input name="duedate" type="date" id="duedate" class="form-control input-lg">
    </div>

    <input type="submit" id="submit" class="btn btn-primary" name="submit">
</form>

<?php

createRows();

?>

<script>

    $(document).ready(function() {
        $("#conditional").hide();
        $('#yes').change(function() {
            if ($('#yes').prop('checked')) {
                $("#conditional").show();
            } else {
                $("#conditional").hide();
            }
        })
        $('#no').change(function() {
            if ($('#no').prop('checked')) {
                $("#conditional").hide();
            }
        })
    })



</script>

<?php include "includes/footer.php" ?>